package com.sbi.tapmarket;

//import android.app.Activity;
import android.app.Application;
//import android.net.Uri;
//import android.os.Bundle;
//import android.text.TextUtils;
//import android.util.Log;
//import com.adjust.sdk.Adjust;
//import com.adjust.sdk.AdjustAttribution;
//import com.adjust.sdk.AdjustConfig;
//import com.adjust.sdk.AdjustEventFailure;
//import com.adjust.sdk.AdjustEventSuccess;
//import com.adjust.sdk.AdjustSessionFailure;
//import com.adjust.sdk.AdjustSessionSuccess;
//import com.adjust.sdk.LogLevel;
//import com.adjust.sdk.OnAttributionChangedListener;
//import com.adjust.sdk.OnDeeplinkResponseListener;
//import com.adjust.sdk.OnEventTrackingFailedListener;
//import com.adjust.sdk.OnEventTrackingSucceededListener;
//import com.adjust.sdk.OnSessionTrackingFailedListener;
//import com.adjust.sdk.OnSessionTrackingSucceededListener;
//import com.google.android.gms.analytics.GoogleAnalytics;
//import com.google.android.gms.analytics.Tracker;
//import com.google.firebase.iid.FirebaseInstanceId;


public class AppDelegate extends Application {


    @Override
    public void onCreate() {
        super.onCreate();

//        String appToken = "cjvsmc1nnabk";
//        String environment = AdjustConfig.ENVIRONMENT_PRODUCTION;
//        AdjustConfig config = new AdjustConfig(this, appToken, environment);
//        config.setAppSecret(1, 593711057, 988156293, 321025198, 2094741253);
//        config.setLogLevel(LogLevel.VERBOSE);
//
//        config.setOnAttributionChangedListener(new OnAttributionChangedListener() { // Set attribution delegate.
//            @Override
//            public void onAttributionChanged(AdjustAttribution attribution) {
//                Log.d("example", "Attribution callback called!");
//                Log.d("example", "Attribution: " + attribution.toString());
//            }
//        });
//
//        config.setOnEventTrackingSucceededListener(new OnEventTrackingSucceededListener() { // Set event success tracking delegate.
//            @Override
//            public void onFinishedEventTrackingSucceeded(AdjustEventSuccess eventSuccessResponseData) {
//                Log.d("example", "Event success callback called!");
//                Log.d("example", "Event success data: " + eventSuccessResponseData.toString());
//            }
//        });
//
//        config.setOnEventTrackingFailedListener(new OnEventTrackingFailedListener() { // Set event failure tracking delegate.
//            @Override
//            public void onFinishedEventTrackingFailed(AdjustEventFailure eventFailureResponseData) {
//                Log.d("example", "Event failure callback called!");
//                Log.d("example", "Event failure data: " + eventFailureResponseData.toString());
//            }
//        });
//
//        config.setOnSessionTrackingSucceededListener(new OnSessionTrackingSucceededListener() { // Set session success tracking delegate.
//            @Override
//            public void onFinishedSessionTrackingSucceeded(AdjustSessionSuccess sessionSuccessResponseData) {
//                Log.d("example", "Session success callback called!");
//                Log.d("example", "Session success data: " + sessionSuccessResponseData.toString());
//            }
//        });
//
//        config.setOnSessionTrackingFailedListener(new OnSessionTrackingFailedListener() { // Set session failure tracking delegate.
//            @Override
//            public void onFinishedSessionTrackingFailed(AdjustSessionFailure sessionFailureResponseData) {
//                Log.d("example", "Session failure callback called!");
//                Log.d("example", "Session failure data: " + sessionFailureResponseData.toString());
//            }
//        });
//
//        config.setOnDeeplinkResponseListener(new OnDeeplinkResponseListener() { // Evaluate deferred deep link to be launched.
//            @Override
//            public boolean launchReceivedDeeplink(Uri deeplink) {
//                Log.d("example", "Deferred deep link callback called!");
//                Log.d("example", "Deep link URL: " + deeplink);
//
//                return true;
//            }
//        });
//
//        config.setSendInBackground(true);
//
//        //Add Session Callback Parameters
//        Adjust.addSessionCallbackParameter("foo", "bar");
//        Adjust.addSessionCallbackParameter("key", "value");
//
//        //Add Session Partner Parameters
//        Adjust.addSessionPartnerParameter("foo", "bar");
//        Adjust.addSessionPartnerParameter("key", "value");
//
//        //Remove Session Callback Parameters
//        Adjust.removeSessionCallbackParameter("foo");
//
//        //Remove Session Partner Parameters
//        Adjust.removeSessionPartnerParameter("key");
//
//        //Remove All Session Callback Parameters
//        Adjust.resetSessionCallbackParameters();
//
//        //Remove All Session Partner Parameters
//        Adjust.resetSessionPartnerParameters();
//
//        Adjust.onCreate(config);
//
//        FirebaseInstanceId fcm = FirebaseInstanceId.getInstance();
//        if (fcm != null) {
//            String refreshToken = fcm.getToken();
//            if (!TextUtils.isEmpty(refreshToken)) {
//                Adjust.setPushToken(refreshToken, getApplicationContext());
//            }
//        }
//        registerActivityLifecycleCallbacks(new AdjustLifecycleCallbacks());
    }

//    private static final class AdjustLifecycleCallbacks implements ActivityLifecycleCallbacks {
//        @Override
//        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
//
//        }
//
//        @Override
//        public void onActivityStarted(Activity activity) {
//
//        }
//
//        @Override
//        public void onActivityResumed(Activity activity) {
//            Adjust.onResume();
//        }
//
//        @Override
//        public void onActivityPaused(Activity activity) {
//            Adjust.onPause();
//        }
//
//        @Override
//        public void onActivityStopped(Activity activity) {
//
//        }
//
//        @Override
//        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
//
//        }
//
//        @Override
//        public void onActivityDestroyed(Activity activity) {
//
//        }
//    }

    /**
     * Google Analytics
     * Gets the default {@link Tracker} for this {@link Application}.
     * @return tracker
     */
//    synchronized public Tracker getDefaultTracker() {
//        GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
//        String TRACKING_ID = "UA-162306560-1";
//        return analytics.newTracker(TRACKING_ID);
//    }

}