package com.sbi.tapmarket.utils;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.sbi.tapmarket.R;
import com.wafflecopter.multicontactpicker.ContactResult;
import com.wafflecopter.multicontactpicker.LimitColumn;
import com.wafflecopter.multicontactpicker.MultiContactPicker;

import java.util.ArrayList;

public class ContactPickerHelper {
    private final int PICK_CONTACT_ACTION_REQUEST = 123;
    private final int READ_CONTACT_PERMISSIONS_REQUEST = 789;

    private Activity activity;
    private ContactPickerListener listener;
    private int limit = 1;

    public ContactPickerHelper(Activity activity, ContactPickerListener listener){
        this.activity = activity;
        this.listener = listener;
    }

    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        switch (reqCode) {
            case (PICK_CONTACT_ACTION_REQUEST):
                if(resultCode == Activity.RESULT_OK) {

                    ArrayList<ContactResult> results = MultiContactPicker.obtainResult(data);

                    if (results.size() > limit) {
                        new AlertDialog.Builder(activity)
                                .setTitle("To many contact selected")
                                .setMessage("Maximum " + limit +  " contacts can be invited. Please try again.")
                                .setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        openContactPicker(limit);
                                    }
                                })
                                .setNegativeButton("Cancel", null)
                                .create()
                                .show();
                    }else{
                        listener.onResult(results);
                    }
                } else if(resultCode == Activity.RESULT_CANCELED){
                    System.out.println("User closed the picker without selecting items.");
                }
                break;
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case READ_CONTACT_PERMISSIONS_REQUEST: {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openContactPicker(limit);
                }
            }
        }
    }

    public void openContactPicker (int limit){
        this.limit = limit;

        if (ContextCompat.checkSelfPermission(this.activity, Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this.activity,
                    new String[]{Manifest.permission.READ_CONTACTS},
                    READ_CONTACT_PERMISSIONS_REQUEST);

            return;
        }


        new MultiContactPicker.Builder(this.activity) //Activity/fragment context
//                .theme(R.style.MyCustomPickerTheme) //Optional - default: MultiContactPicker.Azure
                .hideScrollbar(false) //Optional - default: false
                .showTrack(true) //Optional - default: true
                .searchIconColor(Color.WHITE) //Option - default: White
                .setChoiceMode(MultiContactPicker.CHOICE_MODE_MULTIPLE) //Optional - default: CHOICE_MODE_MULTIPLE
                .handleColor(ContextCompat.getColor(this.activity, R.color.colorPrimary)) //Optional - default: Azure Blue
                .bubbleColor(ContextCompat.getColor(this.activity, R.color.colorPrimary)) //Optional - default: Azure Blue
                .bubbleTextColor(Color.WHITE) //Optional - default: White
                .setTitleText("Select Contestant") //Optional - default: Select Contacts
//                .setSelectedContacts(results) //Optional - will pre-select contacts of your choice. String... or List<ContactResult>
                .setLoadingType(MultiContactPicker.LOAD_ASYNC) //Optional - default LOAD_ASYNC (wait till all loaded vs stream results)
                .limitToColumn(LimitColumn.PHONE) //Optional - default NONE (Include phone + email, limiting to one can improve loading time)
                .setActivityAnimations(
                        android.R.anim.fade_in,
                        android.R.anim.fade_out,
                        android.R.anim.fade_in,
                        android.R.anim.fade_out) //Optional - default: No animation overrides
                .showPickerForResult(PICK_CONTACT_ACTION_REQUEST);
    }

    public interface ContactPickerListener {
        void onResult(ArrayList<ContactResult> result);
    }
}
