package com.sbi.tapmarket.utils;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.BufferedSink;
import okio.Okio;
import okio.Source;


public class ProgressRequestBody extends RequestBody {

    private static final int SEGMENT_SIZE = 2048; // okio.Segment.SIZE

    private final File file;
    private final String contentType;

    public ProgressRequestBody(File file, String contentType) {
        this.file = file;
        this.contentType = contentType;
    }

    @Override
    public long contentLength() {
        return file.length();
    }

    @Override
    public MediaType contentType() {
        return MediaType.parse(contentType);
    }

    @Override
    public void writeTo(BufferedSink sink) throws IOException {
        Source source = null;
        try {
            source = Okio.source(file);
            long total = 0;
            long read;

            while ((read = source.read(sink.buffer(), SEGMENT_SIZE)) != -1) {
                total += read;
                sink.flush();

                final int percent = (int) (total / (float)file.length() * 100);
            }
        } finally {
//            Util.closeQuietly(source);
        }
    }

    public interface ProgressListener {
        void transferred(int num, long transferred, long totalSize);
    }
}
