package com.sbi.tapmarket.utils;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sbi.tapmarket.models.DataChargingContent;
import com.sbi.tapmarket.models.ModelDataUser;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class SessionManager {

    static Gson gson = new Gson();

    public static ModelDataUser saveProfile(Context context, ModelDataUser own){
        ModelDataUser updated = getProfile(context);
        if(updated == null) {
            updated = own;
        }else {

            if (own.msisdn != null)
                updated.msisdn =own.msisdn;

            if (own.user_id != null)
                updated.user_id =own.user_id;

            if (own.nickname != null)
                updated.nickname =own.nickname;

            if (own.token != null)
                updated.token =own.token;
        }

        String json = gson.toJson(updated, ModelDataUser.class);
        PreferenceUtil.getEditor(context).putString("own", json).commit();
        return updated;
    }

    public static ModelDataUser getProfile(Context context){
        String json = PreferenceUtil.getPref(context).getString("own", null);
        if(!TextUtils.isEmpty(json))
            return gson.fromJson(json, ModelDataUser.class);
        else
            return null;
    }

    public static void saveProfileJson(Context context, String list){
        PreferenceUtil.getEditor(context).putString(PreferenceUtil.PROFILE_JSON, list).commit();
    }

    /**
     * Preference Data Charging Content
     * Create Yudha Pratama Putra, 18 sep 2018
     */
    public static void saveChargeArrayList (Context context, ArrayList<DataChargingContent> list){
        Gson gson = new Gson();
        String json = gson.toJson(list);
        PreferenceUtil.getEditor(context).putString(PreferenceUtil.CHARGGING, json).commit();
    }

    /**
     * Preference Data Charging Content
     * Create Yudha Pratama Putra, 18 sep 2018
     */
    public static ArrayList<DataChargingContent> getChargeArrayList (Context context){
        String json = PreferenceUtil.getPref(context).getString(PreferenceUtil.CHARGGING, null);
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<DataChargingContent>>() {}.getType();
        return gson.fromJson(json, type);
    }


}
