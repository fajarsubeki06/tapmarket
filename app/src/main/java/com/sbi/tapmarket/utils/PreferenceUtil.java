package com.sbi.tapmarket.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceUtil {

    private static PreferenceUtil instance;
    private Context context;

    private PreferenceUtil(Context context) {
        this.context = context;
    }

    public static PreferenceUtil init(Context context){
        if (instance == null){
            instance = new PreferenceUtil(context);
        }
        return instance;
    }

    public static PreferenceUtil getInstance(){
        return instance;
    }

    public static final String NAME = "judgeme_preference";
    public static final String AUTH = "auth";
    public static final String CHARGGING = "charge";
    public static final String PROFILE_JSON = "profile";
    public static final String TRIAL_ID = "profile";

    public static SharedPreferences getPref(Context context){
        return context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
    }

    public static SharedPreferences.Editor getEditor(Context context){
        return context.getSharedPreferences(NAME, Context.MODE_PRIVATE).edit();
    }

    // for get base url
    public SharedPreferences getPref(){
        return context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
    }

    public SharedPreferences.Editor getEditor(){
        return context.getSharedPreferences(NAME, Context.MODE_PRIVATE).edit();
    }
}
