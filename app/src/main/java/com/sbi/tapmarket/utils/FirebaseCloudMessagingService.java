package com.sbi.tapmarket.utils;

import android.app.Notification;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.sbi.tapmarket.R;


public class FirebaseCloudMessagingService extends FirebaseMessagingService {

    public String TAG = "FIREBASE MESSAGING";

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        if (remoteMessage == null)
            return;

        Object datas = remoteMessage.getData();
        if (datas!= null) {
            String sCuration = datas.toString();
            if (!TextUtils.isEmpty(sCuration)) {
                if (sCuration.equalsIgnoreCase("{status=Approved}")) {
                    return;
                }
            }
        }

        Log.d(TAG, "From: " + remoteMessage.getFrom());

        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle(remoteMessage.getData().get("title"))
                .setContentText(remoteMessage.getData().get("body"))
                .setSmallIcon(R.mipmap.ic_launcher)
                .build();
        NotificationManagerCompat manager = NotificationManagerCompat.from(getApplicationContext());
        manager.notify(123, notification);

        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {

            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
    }

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        try {
            Log.d(TAG, "Refreshed token: " + s);
            sendRegistrationToServer(s);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void sendRegistrationToServer(String token) {
        Log.d("TOKEN ", token);
    }
}
