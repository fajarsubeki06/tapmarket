package com.sbi.tapmarket.utils;

import com.sbi.tapmarket.models.DataChargingContent;
import com.sbi.tapmarket.models.DataDynamicLink;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;


public interface Services {

    @FormUrlEncoded
    @POST("login")
    Call<APIResponse> getNickName(
            @Field("msisdn") String msisdn,
            @Field("reg_id") String reg_id
    ); // Get data topic subscribe

    @FormUrlEncoded
    @POST("set_profile")
    Call<APIResponse<ArrayList<String>>> setNickName(
            @Field("user_id") String userid,
            @Field("nickname") String nickname,
            @Field("token") String token
    ); // Get data topic subscribe

    @FormUrlEncoded
    @POST("dynamicLink")
    Call<APIResponse<DataDynamicLink>> getDynamicShare(
            @Field("trial_id") String source);

    @FormUrlEncoded
    @POST("List_charging")
    Call<APIResponse<ArrayList<DataChargingContent>>> getListCharging(
            @Field("token") String token,
            @Field("type") String type
    );

    @GET("exampleapi.php")
    Call<List<String>> getProducts();

    @FormUrlEncoded
    @POST("gpay")
    Call<APIResponse<ArrayList<String>>> getSubsGpay(
            @Field("msisdn") String msisdn,
            @Field("type") String type,
            @Field("product_purchased") String product_purchased,
            @Field("canceled") boolean success,
            @Field("period") String period,
            @Field("price") String price
    );

    @FormUrlEncoded
    @POST("transaction")
    Call<APIResponse> sentReceipt(
            @Field("gpayreceipt") String gpayreceipt,
            @Field("token") String token
    );

}
