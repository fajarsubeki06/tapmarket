package com.sbi.tapmarket.utils;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sbi.tapmarket.activitys.PhoneNumberModel;
import com.sbi.tapmarket.models.ContactModel;
import com.sbi.tapmarket.models.ModelDataUser;
import com.tomash.androidcontacts.contactgetter.entity.ContactData;
import com.wafflecopter.multicontactpicker.ContactResult;
import com.wafflecopter.multicontactpicker.RxContacts.PhoneNumber;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class Util {

    public static String formatMSISDNREG(String number) {
        return formatMSISDN(number, "62");
    }

    public static String formatMSISDN(String number, String prefix){
        if(!TextUtils.isEmpty(number) ){

            if (TextUtils.isEmpty(prefix)){
                return "";
            }

            // skip number
            if(number.startsWith("*") || // skip '*'
                    number.startsWith(" ")){ // skip ' '
                return "";
            }

            // get numeric only
            String result = number
                    .replace(" ", "")
                    .replace("-","")
                    .replace("(", "")
                    .replace(")", "");

            prefix = prefix.replace("+", "");

            // replace 0 to +country code
            if (result.startsWith("0")){
                result = result.replaceFirst("0","");
                result = "+"+prefix+result;
            }
            return result;
        }
        return "";
    }

    public static String convertContactResultToJsonArray(ArrayList<ContactResult> results){

        ArrayList<ContactModel> arrPhoneNumber = new ArrayList<>();

        ContactModel contactModel;

        for(ContactResult contact: results){
            contactModel = new ContactModel();
            contactModel.name = contact.getDisplayName();
            contactModel.contact = convertPhoneNumber(contact.getPhoneNumbers());
            arrPhoneNumber.add(contactModel);
        }

        Type type = new TypeToken<ArrayList<ContactModel>>() {}.getType();
        Gson gson = new Gson();
        String result = gson.toJson(arrPhoneNumber, type);

        return result;
    }

    public static String convertMsisdnResultToJsonArray(ArrayList<PhoneNumberModel> results){
        Gson gson = new Gson();
        String result = gson.toJson(results);
        return result;
    }

    public static String convertContactDataToJsonArray(List<ContactData> results){

        ArrayList<ContactModel> arrPhoneNumber = new ArrayList<>();

        ContactModel contactModel;

        for(ContactData contact: results){
            contactModel = new ContactModel();
            contactModel.name = contact.getAccountName();
            contactModel.contact = convertPhoneNumber2(contact.getPhoneList());
            arrPhoneNumber.add(contactModel);
        }

        Type type = new TypeToken<ArrayList<ContactModel>>() {}.getType();
        Gson gson = new Gson();
        String result = gson.toJson(arrPhoneNumber, type);

        return result;
    }

    public static String convertPlayerDataJsonObject(ModelDataUser results){
        Type type = new TypeToken<ModelDataUser>() {}.getType();
        Gson gson = new Gson();
        String result = gson.toJson(results, type);
        return result;
    }

    public static String convertPlayerDataJsonObject(Object results){
        Type type = new TypeToken<Object>() {}.getType();
        Gson gson = new Gson();
        String result = gson.toJson(results, type);
        return result;
    }

    private static ArrayList<PhoneNumberModel> convertPhoneNumber(List<PhoneNumber> phoneNumber){
        ArrayList<PhoneNumberModel> data = new ArrayList<>();
        PhoneNumberModel model;
        for(PhoneNumber number: phoneNumber){
            model = new PhoneNumberModel();
            model.number = Util.formatMSISDN(number.getNumber(), "62");
            model.type = number.getTypeLabel();
            data.add(model);
        }
        return data;
    }

    private static ArrayList<PhoneNumberModel> convertPhoneNumber2(List<com.tomash.androidcontacts.contactgetter.entity.PhoneNumber> phoneNumber){
        ArrayList<PhoneNumberModel> data = new ArrayList<>();
        PhoneNumberModel model;
        for(com.tomash.androidcontacts.contactgetter.entity.PhoneNumber number: phoneNumber){
            model = new PhoneNumberModel();
            model.number = Util.formatMSISDN(number.getMainData(), "62");
            model.type = number.getLabelName();
            data.add(model);
        }
        return data;
    }

    public static boolean isWifiConnected(Context context){
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return mWifi.isConnected();
    }

    public static void hideKeyboard(Activity activity){
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
    }

    public static void showKeyboard(Activity activity, View view){
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

}
