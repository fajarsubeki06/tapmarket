package com.sbi.tapmarket.utils;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.MediaStore;

import androidx.annotation.NonNull;

/**
 * Created by Yudha Pratama Putra on 08/18/18.
 */
public class ImageSelector {
    final int REQUEST_IMAGE_CAPTURE = 1324;
    final int REQUEST_IMAGE_GALLERY = 2435;
    final int REQUEST_PERMISSIONS = 7656;

    final String [] galleryPermissions = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    final String [] cameraPermissions = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
    };

    Activity mActivity;
    ImageSelectorDelegate mDelegate;
    Uri mCameraUri;
    int mAction = 0;

    public static ImageSelector create(Activity activity, ImageSelectorDelegate delegate){
        return new ImageSelector(activity, delegate);
    }

    private ImageSelector(Activity activity, ImageSelectorDelegate delegate){
        this.mActivity = activity;
        this.mDelegate = delegate;
    }

    public void openGallery(){
        mAction = 0;

        if(!PermissionUtil.hashPermission(mActivity, galleryPermissions)){
            PermissionUtil.requestPermission(mActivity, galleryPermissions, REQUEST_PERMISSIONS);
            return;
        }

        Intent pickerIntent = new Intent();
        pickerIntent.setAction(Intent.ACTION_GET_CONTENT);
        pickerIntent.addCategory(Intent.CATEGORY_OPENABLE);
        pickerIntent.setTypeAndNormalize("image/*");
        mActivity.startActivityForResult(pickerIntent, REQUEST_IMAGE_GALLERY);
    }

    public void captureImage(){
        mAction = 1;

        if(!PermissionUtil.hashPermission(mActivity, cameraPermissions)){
            PermissionUtil.requestPermission(mActivity, cameraPermissions, REQUEST_PERMISSIONS);
            return;
        }

        if(!mActivity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)){
            // TODO show error message
            return;
        }

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(mActivity.getPackageManager()) != null) {
            mCameraUri = mActivity.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new ContentValues());

            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (cameraIntent.resolveActivity(mActivity.getPackageManager()) != null) {
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCameraUri);
                mActivity.startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);
            }
            else{
                // TODO show error message
            }
        }
        else {
            // TODO show error message
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK && mCameraUri != null){
            // send callback
            mDelegate.onImageSelected(mCameraUri);
            return;
        }

        if(requestCode == REQUEST_IMAGE_GALLERY && resultCode == Activity.RESULT_OK){
            // send callback
            mDelegate.onImageSelected(data.getData());
            return;
        }

        // TODO show error message
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults){
        if(requestCode == REQUEST_PERMISSIONS && PermissionUtil.hashPermission(mActivity, permissions)){
            if(mAction == 1)
                captureImage();
            else
                openGallery();
        }
        else{
            // TODO show permission required message
        }
    }

    public interface ImageSelectorDelegate{
        void onImageSelected(Uri uri);
    }
}
