package com.sbi.tapmarket;

import android.app.Application;

//import com.google.android.gms.analytics.HitBuilders;
//import com.google.android.gms.analytics.Tracker;

public class AnalyticsApplication {

    private static AnalyticsApplication instance;
    private AppDelegate appDelegate;

    private AnalyticsApplication(Application application) {
        appDelegate = (AppDelegate) application;
    }

    public static AnalyticsApplication getInstance(Application application){
        if (instance == null){
            instance = new AnalyticsApplication(application);
        }
        return instance;
    }

    public void sendScreen(String screenName){
//        Tracker tracker = appDelegate.getDefaultTracker();
//        tracker.setScreenName(screenName);
//        tracker.send(new HitBuilders.ScreenViewBuilder().setCampaignParamsFromUrl("").build());
    }

    public void sendEventWithScreen(String screenName, String cat, String action, String label, long val){
//        Tracker tracker = appDelegate.getDefaultTracker();
//        tracker.setScreenName(screenName);
//        tracker.send(new HitBuilders.EventBuilder()
//                .setCategory(cat)
//                .setAction(action)
//                .setLabel(label)
//                .setValue(val)
//                .build());
    }

    public void sendEventWithScreen(String screenName, String cat, String action, String label){
//        Tracker tracker = appDelegate.getDefaultTracker();
//        tracker.setScreenName(screenName);
//        tracker.send(new HitBuilders.EventBuilder()
//                .setCategory(cat)
//                .setAction(action)
//                .setLabel(label)
//                .build());
    }
}
