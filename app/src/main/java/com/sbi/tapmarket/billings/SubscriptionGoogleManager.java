package com.sbi.tapmarket.billings;


//import com.android.billingclient.api.BillingClient;
//import com.android.billingclient.api.BillingClientStateListener;
//import com.android.billingclient.api.BillingFlowParams;
//import com.android.billingclient.api.Purchase;
//import com.android.billingclient.api.PurchasesUpdatedListener;
//import com.android.billingclient.api.SkuDetails;
//import com.android.billingclient.api.SkuDetailsParams;


public class SubscriptionGoogleManager {

//    private BillingClient mBillingClient;
//    private List<String> mSKUList;
//    private SubscriptionGoogleListener mListener;
//    private SkuDetails skuDetails;
//    private Activity activity;
//
//    private String priceLabel = "";
//    private String purchasedJson;

//    public SubscriptionGoogleManager (Activity activity, SubscriptionGoogleListener listener){
//        this.activity = activity;
//        this.mListener = listener;
//        initBillingClient();
//    }
//
//    private void initBillingClient() {
//        mBillingClient = BillingClient
//                .newBuilder(activity)
//                .setListener(mPurchaseUpdateListener)
//                .build();
//        mBillingClient.startConnection(new BillingClientStateListener() {
//            @Override
//            public void onBillingSetupFinished(int responseCode) {
//                if (responseCode == BillingClient.BillingResponse.OK) {
//                    mListener.onBillingConnected(true);
//                }else{
//                    mListener.onBillingConnected(false);
//                }
//
//                if (mSKUList != null && mSKUList.size()>0) {
//                    String sku_id = mSKUList.get(0);
//                    if (!TextUtils.isEmpty(sku_id)) {
//                        checkSubsHistory(sku_id);
//                    }
//                }
//            }
//
//            @Override
//            public void onBillingServiceDisconnected() {
//                mListener.onBillingConnected(false);
//            }
//        });
//    }
//
//    private PurchasesUpdatedListener mPurchaseUpdateListener = (responseCode, purchases) -> {
//        if (responseCode == BillingClient.BillingResponse.OK){
//            if (purchases != null) {
//                mListener.onSubscribe(purchases.get(0).getOriginalJson(),
//                        String.valueOf(skuDetails.getPriceAmountMicros() / 1000000),
//                        skuDetails.getSubscriptionPeriod());
//                Toast.makeText(activity, "Success subscribe", Toast.LENGTH_SHORT).show();
//            }
//        }
//        else if (responseCode == BillingClient.BillingResponse.USER_CANCELED){
//            System.out.println("BILLING | startConnection | RESULT: "+responseCode);
//        }
//        else{
//            System.out.println("BILLING | onBillingServiceDisconnected | DISCONNECTED");
//        }
//    };
//
//    public void loadSKU(String skuID){
//        if (mBillingClient.isReady()) {
//            mSKUList = Arrays.asList(skuID);
//
//            SkuDetailsParams params = SkuDetailsParams
//                    .newBuilder()
//                    .setSkusList(mSKUList)
//                    .setType(BillingClient.SkuType.SUBS)
//                    .build();
//
//            mBillingClient.querySkuDetailsAsync(params, (int responseCode, List<SkuDetails> skuDetailsList)->{
//
//                if(responseCode == BillingClient.BillingResponse.OK){
//                    if(skuDetailsList.size() > 0) {
//                        skuDetails = skuDetailsList.get(0);
//
//                        String period = skuDetails.getSubscriptionPeriod();//*****
//                        String periodLabel = "Day";
//                        if(period.toLowerCase().contains("w"))
//                            periodLabel = "Week";
//                        else if(period.toLowerCase().contains("m"))
//                            periodLabel = "Month";
//                        else if(period.toLowerCase().contains("y"))
//                            periodLabel = "Year";
//
//                        priceLabel = skuDetails.getPrice() + "/ "+ periodLabel + ", autorenewal";
//                        mListener.onProductLoaded();
//                    }
//
//                }else{
//                    System.out.println("Can't querySkuDetailsAsync, responseCode: "+responseCode);
//                }
//            });
//        } else {
//            System.out.println("Billing Client not ready");
//        }
//    }
//
//    public void performSubscribe(){
//        BillingFlowParams billingFlowParams = BillingFlowParams
//                .newBuilder()
//                .setSkuDetails(skuDetails)
//                .build();
//        mBillingClient.launchBillingFlow(activity, billingFlowParams);
//    }
//
//    private void checkSubsHistory(String skuID){
//        purchasedJson = null;
//        Purchase.PurchasesResult purchasesResult = mBillingClient.queryPurchases(BillingClient.SkuType.SUBS);
//        if(purchasesResult != null && purchasesResult.getPurchasesList() != null && purchasesResult.getPurchasesList().size() > 0){
//            for(Purchase purchase:purchasesResult.getPurchasesList()){
//                String sku = purchase.getSku();
//                if(!TextUtils.isEmpty(sku) && sku.equals(skuID)){
//                    purchasedJson = purchase.getOriginalJson();
//                    mListener.onSubsChecked(true);
//                    return;
//                }
//            }
//        }
//        mListener.onSubsChecked(false);
//    }
//
//
//    public static abstract class SubscriptionGoogleListener {
//        protected void onBillingConnected(boolean success){}
//        protected void onProductLoaded(){}
//        protected void onSubscribe(String json, String price, String period){}
//        protected void onUpdateSubsStatus(boolean success, String json, String price, String period){}
//        protected void onSubsChecked(boolean exists){}
//    }

}