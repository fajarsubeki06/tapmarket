package com.sbi.tapmarket.activitys;

public class PhoneNumberModel {
    public String number;
    public String type;

    public PhoneNumberModel(){}

    public PhoneNumberModel(String number, String type) {
        this.number = number;
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
