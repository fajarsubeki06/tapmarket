package com.sbi.tapmarket.activitys;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sbi.tapmarket.R;
import com.sbi.tapmarket.adapter.PopUpSubsAdapter;
import com.sbi.tapmarket.models.DataChargingContent;
import com.sbi.tapmarket.utils.SessionManager;

import java.util.ArrayList;


public class PopUpSubsActivity extends AppCompatActivity {

    private RecyclerView rvRadio;
    public static int RESULT_CODE = 101;
    public static String STR_EXTRA_VALUE = "popup_dismiss";
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pop_up_subs);

        rvRadio = findViewById(R.id.rvRadioSubs);
        TextView txtTittle = findViewById(R.id.txtTittle);
        TextView textVote = findViewById(R.id.txtVote);
        TextView textVoteValue = findViewById(R.id.txtVoteValue);
        TextView textInfo = findViewById(R.id.textInfo);
        TextView txtPpn = findViewById(R.id.txtPpn);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null){

            String title = bundle.getString("title");
            String des_coin = bundle.getString("des_coin");
            String desc_info = bundle.getString("desc_info");
            String bottom_ppn = bundle.getString("bottom_ppn");
            String coin_value = bundle.getString("coin_value");

            if (!TextUtils.isEmpty(title)
                    && !TextUtils.isEmpty(des_coin)
                    && !TextUtils.isEmpty(desc_info)
                    && !TextUtils.isEmpty(bottom_ppn)
                    && !TextUtils.isEmpty(coin_value)
            ) {

                txtTittle.setText(title);

                assert coin_value != null;
                int coin = Integer.parseInt(coin_value);

                if (coin == 0){
                    textVoteValue.setTextColor(getResources().getColor(android.R.color.holo_red_light));
                }else{
                    textVoteValue.setText(String.valueOf(coin));
                }

                textVote.setText(des_coin);
                textInfo.setText(desc_info);
                txtPpn.setText(bottom_ppn);

            }

        }

        initRecycler();

        Intent intent = new Intent();
        intent.putExtra(STR_EXTRA_VALUE,100);
        setResult(RESULT_CODE,intent);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage("Processing...");
        progressDialog.setIndeterminate(true);

    }

    @SuppressLint("WrongConstant")
    private void initRecycler() {
        LinearLayoutManager layout = new LinearLayoutManager(this);
        layout.setOrientation(LinearLayoutManager.VERTICAL);
        rvRadio.setLayoutManager(layout);
        ArrayList<DataChargingContent> getSubData = SessionManager.getChargeArrayList(getApplicationContext());
        if (getSubData != null && getSubData.size()>0){
            PopUpSubsAdapter ar = new PopUpSubsAdapter(getSubData,PopUpSubsActivity.this);
            rvRadio.setAdapter(ar);
        }
    }

}