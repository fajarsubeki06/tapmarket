package com.sbi.tapmarket.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sbi.tapmarket.R;
import com.sbi.tapmarket.models.ModelDataUser;
import com.sbi.tapmarket.utils.APIResponse;
import com.sbi.tapmarket.utils.PreferenceUtil;
import com.sbi.tapmarket.utils.ServicesFactory;
import com.sbi.tapmarket.utils.SessionManager;
import com.sbi.tapmarket.utils.Util;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PopupSetName extends AppCompatActivity implements View.OnClickListener {

    private Button btnSetNickName;
    private String pMsisdn;
    private String pUserId;
    private String pToken;
    private EditText edtSetNickName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popup_set_name);

        edtSetNickName = findViewById(R.id.edtSetNickName);
        btnSetNickName = findViewById(R.id.btnSetNickName);
        btnSetNickName.setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            pMsisdn= bundle.getString("msisdn");
            pUserId= bundle.getString("user_id");
            pToken = bundle.getString("token");
        }

    }

    @Override
    public void onClick(View v) {
        if (btnSetNickName == v){
            String sNickname = edtSetNickName.getText().toString();
            if (!TextUtils.isEmpty(sNickname)) {
                if (!TextUtils.isEmpty(pMsisdn) && !TextUtils.isEmpty(pUserId) && !TextUtils.isEmpty(pToken)) {
                    setNickName(pMsisdn, pUserId, sNickname, pToken);
                }
            }else {
                Toast.makeText(PopupSetName.this, "Nama mu tidak boleh kosong", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void setNickName(final String pMsisdn, final String pUserId, final String pNickname, final String pToken) {
        Call<APIResponse<ArrayList<String>>> call = ServicesFactory.getService().setNickName(pUserId,pNickname,pToken);
        call.enqueue(new Callback<APIResponse<ArrayList<String>>>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse<ArrayList<String>>> call, @NonNull Response<APIResponse<ArrayList<String>>> response) {
                if (response.body() != null && response.isSuccessful()){
                    int code = response.code();
                    String message = TextUtils.isEmpty(response.message())?"":response.message();

                    if (code == 200 && message.equals("OK")){

                        String ccode = response.body().error_message;
                        System.out.println(ccode);
                        if (!TextUtils.isEmpty(ccode) && ccode.equalsIgnoreCase("Duplicate Nickname")){
                            FirebaseAuth.getInstance().signOut();
                            customToast("Nama sudah terpakai");
                        }else {

                            ModelDataUser own = new ModelDataUser();
                            own.msisdn = pMsisdn;
                            own.user_id = pUserId;
                            own.nickname = pNickname;
                            own.token = pToken;

                            SessionManager.saveProfile(getApplicationContext(), own);
                            startActivity(new Intent(PopupSetName.this, MainActivity.class));
                            PreferenceUtil.getEditor(PopupSetName.this).putBoolean(PreferenceUtil.AUTH, true).commit();

                            String jsonDataString =
                                    PreferenceUtil.getPref(getApplicationContext())
                                            .getString(PreferenceUtil.PROFILE_JSON, "");

                            if (!TextUtils.isEmpty(jsonDataString) && !TextUtils.isEmpty(pNickname)) {
                                try {
                                    JsonParser jsonParser = new JsonParser();
                                    assert jsonDataString != null;
                                    JsonObject jsonObject = (JsonObject) jsonParser.parse(jsonDataString);
                                    jsonObject.addProperty("nickname", pNickname);
                                    String results = Util.convertPlayerDataJsonObject(jsonObject);
                                    SessionManager.saveProfileJson(getApplicationContext(), results);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }


                        }

                    } else {
                        customToast("Error response data server");
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<APIResponse<ArrayList<String>>> call, @NonNull Throwable t) {
                customToast("popup set name, on failure");
            }
        });
    }


    private void customToast(String message){
        Toast.makeText(PopupSetName.this, message, Toast.LENGTH_SHORT).show();
    }

}
