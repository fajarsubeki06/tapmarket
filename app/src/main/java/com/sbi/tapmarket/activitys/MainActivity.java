package com.sbi.tapmarket.activitys;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Base64;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.sbi.tapmarket.R;
import com.sbi.tapmarket.utils.ImageSelector;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.util.List;
import java.util.Objects;

import dha.code.sbilauncherlibrary.WebAppInterface;
import dha.code.sbilauncherlibrary.WebChromeClientExtension;
import dha.code.sbilauncherlibrary.WebViewClientExtension;
import dha.code.sbilauncherlibrary.contract.MainContract;
import dha.code.sbilauncherlibrary.interactor.IntractorImpl;
import dha.code.sbilauncherlibrary.presenters.UploadImagePresenterImpl;
import dha.code.sbilauncherlibrary.utilities.PreferenceLibrary;


public class MainActivity extends AppCompatActivity {

    private WebView webView;
    private UploadImagePresenterImpl uploadImagePresenter;
    private ImageSelector mImageSelector;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        webView = findViewById(R.id.web_view);
        initImageSelector();
        initViewAction();
    }

    @Override
    protected void onStart() {
        super.onStart();
        reloadUrl("javascript:inForeground()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        reloadUrl("javascript:inBackground()");
    }

    /**
     * Load URL
     *
     * */
    private void loadUrl() {
        String msisdn = PreferenceLibrary.getPref(MainActivity.this).getString(PreferenceLibrary.PREF_MSISDN,"");
        String url = "http://117.54.3.28:11680/layout/ulertenggo/mobile/?msisdn="+msisdn;
        webView.loadUrl(url);
    }

    /**
     * Reload URL
     * */
    private void reloadUrl(String fuction) {
        if (webView != null && !TextUtils.isEmpty(fuction)) {
            webView.loadUrl(fuction);
        }
    }

    // *
    // Get image from activity result
    // *
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (result != null) {
                if (resultCode == RESULT_OK) {
                    if (result.getUri() != null){
                        Uri resultsUri = result.getUri();
                        if (resultsUri.getPath() != null){
                            parsingImageFile(resultsUri);
                        }
                    }
                }
            }

        } else {
            mImageSelector.onActivityResult(requestCode, resultCode, data);
        }
    }

    // *
    // Get image request permission
    // *
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mImageSelector.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /**
     * Init View
     * */
    @SuppressLint("SetJavaScriptEnabled")
    private void initViewAction() {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAppCacheEnabled(false);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.setWebViewClient(new WebViewClientExtension());
        webView.setWebChromeClient(new WebChromeClientExtension());
        parsingJavaScriptInterface(webView);
        mProgressDialog = new ProgressDialog(MainActivity.this);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        loadUrl();
        uploadImageListener();
    }

    /**
     * Image selector
     * */
    private void initImageSelector() {
        mImageSelector = ImageSelector.create(MainActivity.this, uri ->
                CropImage.activity(uri).setAspectRatio(1, 1)
                        .start(MainActivity.this));
    }

    /**
     * Parsing Image File
     * */
    private void parsingImageFile(Uri uri) {
        final File proPicPath = new File(Objects.requireNonNull(uri.getPath()));
        String _msisdn = PreferenceLibrary.getPref(this).getString(PreferenceLibrary.PREF_MSISDN,"");
        if(!TextUtils.isEmpty(_msisdn))
            uploadImagePresenter.setUploadImage(MainActivity.this, proPicPath, _msisdn);
    }

    /**
     * Function Convert Base64 to String
     * */
//    private String encodeImage(Bitmap bm) {
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        bm.compress(Bitmap.CompressFormat.JPEG,100,baos);
//        byte[] b = baos.toByteArray();
//        return Base64.encodeToString(b, Base64.DEFAULT);
//    }

    public static String fromBase64(String message) {
        byte[] data = Base64.decode(message, Base64.DEFAULT);
        return Base64.encodeToString(data, Base64.DEFAULT);
    }

    private void parsingImageFile(String uri) {
        String _uri = fromBase64(uri);
        String script = "javascript:imageSelected(uri)";
        script = script.replace("uri", "'" + _uri + "'");
        if (!TextUtils.isEmpty(script)) {
            reloadUrl(script);
        }
    }

    /**
     * Upload Image Listener
     * */
    private void uploadImageListener() {
        uploadImagePresenter = new UploadImagePresenterImpl(new MainContract.UploadImageView() {
            @Override
            public void isProgress() {
                if (mProgressDialog != null)
                    mProgressDialog.show();
            }

            @Override
            public void isFailure() {
                if (mProgressDialog != null)
                    mProgressDialog.dismiss();
            }

            @Override
            public void isSuccess(String _url) {
                if (mProgressDialog != null) {
                    mProgressDialog.dismiss();
                }
                parsingImageFile(_url);
            }
        }, new IntractorImpl());
    }

    /**
     * Parsing JavaScript Interface
     * */
    private void parsingJavaScriptInterface(WebView webView) {
        if (webView == null) return;

        webView.addJavascriptInterface(new WebAppInterface(MainActivity.this, MainActivity.this, new MainContract.WebResponseView() {
            @Override
            public void onSuccessShareSosmed(List<Intent> targetedShareIntents, String title) {
                Intent chooserIntent = Intent.createChooser(targetedShareIntents.remove(0), title);
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[0]));
                startActivity(chooserIntent);
            }

            @Override
            public void onError() {

            }

            @Override
            public void onSelectedImage(String parsing) {
                if (parsing.equalsIgnoreCase("gallery")) {
                    mImageSelector.openGallery();
                }else{
                    mImageSelector.captureImage();
                }
            }
        }), "Android");
    }

}
