package com.sbi.tapmarket.activitys;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.chaos.view.PinView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.annotations.NotNull;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.sbi.tapmarket.R;
import com.sbi.tapmarket.models.ModelDataUser;
import com.sbi.tapmarket.utils.APIResponse;
import com.sbi.tapmarket.utils.PreferenceUtil;
import com.sbi.tapmarket.utils.ServicesFactory;
import com.sbi.tapmarket.utils.SessionManager;
import com.sbi.tapmarket.utils.Util;

import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import dha.code.sbilauncherlibrary.utilities.PreferenceLibrary;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PhoneAuthentication extends AppCompatActivity implements View.OnClickListener {

    private TextView tvPhoneNumber;
    private TextView tvWrongNumber;
    private Button btnVerification;
    private PinView pinViewCode;
    private TextView tvTimeCount;
    private TextView tvResendCode;
    private ProgressBar progressBar;
    private FirebaseAuth auth;
    private String smsCode;
    private String mVerificationID;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private Timer mTimer;
    private int mCounter = 60;
    private ImageView imgIconAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_authentication);

        auth = FirebaseAuth.getInstance();

        tvPhoneNumber = findViewById(R.id.tvPhoneNumber);
        tvWrongNumber = findViewById(R.id.tvWrongNumber);
        btnVerification = findViewById(R.id.btnVerification);
        pinViewCode = findViewById(R.id.pinViewCode);
        tvTimeCount = findViewById(R.id.tvTimeCount);
        tvResendCode = findViewById(R.id.tvResendCode);
        progressBar = findViewById(R.id.progress_bar);
        imgIconAuth = findViewById(R.id.imgIconAuth);

        pinViewCode.setInputType(InputType.TYPE_CLASS_TEXT);

        Glide.with(PhoneAuthentication.this).asBitmap().load(getResources()
                .getIdentifier("buble_auth", "drawable", getPackageName())).into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(@NotNull final Bitmap bitmap, Transition<? super Bitmap> transition) {
                final int w = bitmap.getWidth();
                final int h = bitmap.getHeight();
                imgIconAuth.post(() -> {
                    int ivW = imgIconAuth.getMeasuredWidth();
                    imgIconAuth.getLayoutParams().height = ivW * h / w;
                    imgIconAuth.requestLayout();
                    imgIconAuth.setImageBitmap(bitmap);
                });
            }
        });

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String parsMsisdn = bundle.getString("msisdn");
            if (!TextUtils.isEmpty(parsMsisdn)) {
                tvPhoneNumber.setText(parsMsisdn);
                sendVerificationCode(parsMsisdn);
            }
        }

        initClickListener();
        initTimer();
    }

    private void initTimer() {
        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                mCounter--;
                Handler mHandler = new Handler(Looper.getMainLooper());
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            tvTimeCount.setText(String.valueOf(mCounter));
                        } catch (Exception e) {
                            return;
                        }
                    }
                });

                if (mCounter == 0) {
                    mCounter = 60;
                    mTimer.cancel();
                }
            }
        }, 0, 1000);
    }

    private void initClickListener() {
        btnVerification.setOnClickListener(this);
        tvResendCode.setOnClickListener(this);
        tvWrongNumber.setOnClickListener(this);
    }

    private void sendVerificationCode(String parsMsisdn) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                parsMsisdn,
                60,
                TimeUnit.SECONDS,
                PhoneAuthentication.this,
                mCallbacks
        );
    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            if (phoneAuthCredential != null && phoneAuthCredential.getSmsCode() != null) {
                smsCode = phoneAuthCredential.getSmsCode();
                if (!TextUtils.isEmpty(smsCode)) {
                    pinViewCode.setText(smsCode);
                    validateShowProgress(true);
                    verifyVerificationCode(smsCode);
                }
            } else {
                signInWithPhoneAuthCredential(phoneAuthCredential);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            showSnackBar("VerificationFailed");
            stopTimer();
        }

        @Override
        public void onCodeSent(String verificationID, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            mVerificationID = verificationID;
            mResendToken = forceResendingToken;
        }

        @Override
        public void onCodeAutoRetrievalTimeOut(String s) {
            super.onCodeAutoRetrievalTimeOut(s);
            showSnackBar("Request Time Out...");
            stopTimer();
        }
    };

    private void verifyVerificationCode(String smsCode) {
        if (!TextUtils.isEmpty(mVerificationID) && mVerificationID != null) {
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationID, smsCode);
            signInWithPhoneAuthCredential(credential);
        } else {
            showSnackBar("Verification code invalid...");
            stopTimer();
        }
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        auth.signInWithCredential(credential)
                .addOnCompleteListener(PhoneAuthentication.this, task -> {
                    String msisdnAuth;
                    if (task.isSuccessful()) {
                        msisdnAuth = Objects.requireNonNull(Objects.requireNonNull(task.getResult()).getUser()).getPhoneNumber();
                        if (!TextUtils.isEmpty(msisdnAuth) && msisdnAuth != null) {
                            stopTimer();
                            msisdnAuth = msisdnAuth.replace("+", "");
                            PreferenceLibrary.getEditor(this).putString(PreferenceLibrary.PREF_MSISDN,msisdnAuth).apply();
                            PreferenceUtil.getEditor(PhoneAuthentication.this).putBoolean(PreferenceUtil.AUTH, true).commit();
                            startActivity(new Intent(PhoneAuthentication.this, MainActivity.class));
                            finish();
                            // checkNickName(msisdnAuth);
                        } else {
                            showSnackBar("Authentication number invalid...");
                            stopTimer();
                        }
                    } else {
                        if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                            showSnackBar("Invalid code entered...");
                            stopTimer();
                        }
                    }
                });
    }

    private void showSnackBar(String s) {
        if (!isFinishing())
            return;
        try {
            Snackbar.make(findViewById(android.R.id.content), s, Snackbar.LENGTH_LONG).show();
        } catch (Exception e) {
            return;
        }
    }

    @Override
    public void onClick(View v) {
        if (btnVerification == v) {
            String pinValue = pinViewCode.getText().toString();
            if (!TextUtils.isEmpty(pinValue)) {
                verifyVerificationCode(pinValue);
                validateShowProgress(true);
            } else {
                Toast.makeText(this, "Kode tidak boleh kosong", Toast.LENGTH_SHORT).show();
            }

            InputMethodManager input = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (input != null) {
                input.hideSoftInputFromWindow(pinViewCode.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
            }
        } else if (tvResendCode == v) {
            System.out.println("Resend code");
            String msisdn = tvPhoneNumber.getText().toString();
            if (!TextUtils.isEmpty(msisdn) && mResendToken != null) {
                initTimer();
                resendVerificationCode(msisdn);
                validateResendCode(false);
            } else {
                showSnackBar("Something wrong or check you connection...");
            }
        } else if (tvWrongNumber == v) {
            startActivity(new Intent(PhoneAuthentication.this, PhoneRegister.class));
            finish();
        }
    }

    private void resendVerificationCode(String msisdn) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(msisdn, 60, TimeUnit.SECONDS, PhoneAuthentication.this, mCallbacks, mResendToken);
    }

    private void checkNickName(final String msisdn) {

        String tokenFirebase = logToken();
        Call<APIResponse> call = ServicesFactory.getService().getNickName(msisdn,tokenFirebase);
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse> call, @NonNull Response<APIResponse> response) {
                if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()) {

                    Object obj = response.body().data;
                    if (obj != null) {
                        String result = Util.convertPlayerDataJsonObject(obj);
                        if (!TextUtils.isEmpty(result)) {
                            SessionManager.saveProfileJson(PhoneAuthentication.this, result);
                            ModelDataUser data = new Gson().fromJson(result, ModelDataUser.class);
                            if (data != null) {

                                String strUserId = data.getUser_id();
                                String strNickName = data.getNickname();
                                String strToken = data.getToken();

                                if (TextUtils.isEmpty(strNickName)) {
                                    if (!TextUtils.isEmpty(strUserId) && !TextUtils.isEmpty(strToken)) {
                                        startActivity(new Intent(PhoneAuthentication.this, PopupSetName.class)
                                                .putExtra("msisdn", msisdn)
                                                .putExtra("user_id", strUserId)
                                                .putExtra("token", strToken)
                                        );
                                    }
                                } else {
                                    ModelDataUser own = new ModelDataUser();
                                    own.msisdn = msisdn;
                                    own.user_id = strUserId;
                                    own.nickname = strNickName;
                                    own.token = strToken;

                                    SessionManager.saveProfile(getApplicationContext(), own);
                                    PreferenceUtil.getEditor(PhoneAuthentication.this).putBoolean(PreferenceUtil.AUTH, true).commit();
                                    startActivity(new Intent(PhoneAuthentication.this, MainActivity.class).putExtra("msisdn", msisdn));
                                    finish();
                                }

                            }

                        }
                    }

                }else{
                    System.out.println("error");
                }
            }

            @Override
            public void onFailure(@NonNull Call<APIResponse> call, @NonNull Throwable t) {
                Log.e("err", "phone auth, on failure");
            }
        });

    }

    private String logToken(){
        String tokenFirebase = "";
        FirebaseInstanceId fcm = FirebaseInstanceId.getInstance();
        if (fcm != null) {
            tokenFirebase = TextUtils.isEmpty(fcm.getToken())?"":fcm.getToken();

        }
        return tokenFirebase;
    }

    private void stopTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            validateResendCode(true);
            validateShowProgress(false);
        }
    }

    private void validateResendCode(boolean isResend) {
        if (!PhoneAuthentication.this.isFinishing()) {
            if (isResend) {
                tvResendCode.setTextColor(getResources().getColor(R.color.red));
                tvResendCode.setEnabled(true);
                tvTimeCount.setVisibility(View.GONE);
                mCounter = 60;
            } else {
                tvResendCode.setTextColor(getResources().getColor(R.color.grey_ACACACF));
                tvResendCode.setEnabled(false);
                tvTimeCount.setVisibility(View.VISIBLE);
            }
        }
    }

    private void validateShowProgress(boolean b) {
        if (!PhoneAuthentication.this.isFinishing()) {
            if (b) {
                progressBar.setVisibility(View.VISIBLE);
                btnVerification.setVisibility(View.INVISIBLE);
            } else {
                progressBar.setVisibility(View.INVISIBLE);
                btnVerification.setVisibility(View.VISIBLE);
            }
        }
    }
}
