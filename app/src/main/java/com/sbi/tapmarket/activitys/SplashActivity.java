package com.sbi.tapmarket.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.sbi.tapmarket.R;
import com.sbi.tapmarket.utils.PreferenceUtil;


public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ImageView imgLogo = findViewById(R.id.imgLogo);
        ImageView imgBackground = findViewById(R.id.imgBackground);

        Glide.with(SplashActivity.this).load(getResources()
                .getIdentifier("logo_ulartenggo", "drawable", getPackageName()))
                .into(imgLogo);

        Glide.with(SplashActivity.this).load(getResources()
                .getIdentifier("bg_splash_screen", "drawable", getPackageName()))
                .into(imgBackground);

        new Handler().postDelayed(() -> {

            boolean isAuth = PreferenceUtil.getPref(SplashActivity.this).getBoolean(PreferenceUtil.AUTH, false);
            if (isAuth){
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();
            } else {
                startActivity(new Intent(SplashActivity.this, PhoneRegister.class));
                finish();
            }

        }, 2000);

    }
}
