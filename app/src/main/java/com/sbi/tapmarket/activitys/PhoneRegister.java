package com.sbi.tapmarket.activitys;


import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.firebase.database.annotations.NotNull;
import com.sbi.tapmarket.R;
import com.sbi.tapmarket.utils.Util;


public class PhoneRegister extends AppCompatActivity {

    private EditText etPhoneNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_register);

        ImageView imgLogo = findViewById(R.id.imgLogo);
        etPhoneNo = findViewById(R.id.etPhoneNo);
        Button btnReqToken = findViewById(R.id.btnReqToken);

        Glide.with(PhoneRegister.this).asBitmap().load(getResources()
                .getIdentifier("bublbe_text_register", "drawable", getPackageName())).into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(@NotNull final Bitmap bitmap, Transition<? super Bitmap> transition) {
                final int w = bitmap.getWidth();
                final int h = bitmap.getHeight();
                imgLogo.post(() -> {
                    int ivW = imgLogo.getMeasuredWidth();
                    imgLogo.getLayoutParams().height = ivW * h / w;
                    imgLogo.requestLayout();
                    imgLogo.setImageBitmap(bitmap);
                });
            }
        });


        btnReqToken.setOnClickListener(v -> {
            String phoneNo = etPhoneNo.getText().toString();
            String msisdn = Util.formatMSISDNREG(phoneNo);
            if (!TextUtils.isEmpty(msisdn)) {
                startActivity(new Intent(PhoneRegister.this, PhoneAuthentication.class).putExtra("msisdn", msisdn));
                finish();
            } else {
                Toast.makeText(PhoneRegister.this, "Nomor telepon tidak boleh kosong...", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
