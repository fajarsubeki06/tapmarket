package com.sbi.tapmarket.activitys;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Telephony;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.annotations.NotNull;
import com.sbi.tapmarket.R;
import com.sbi.tapmarket.adapter.IBaseAdapterListener;
import com.sbi.tapmarket.adapter.ShareListAdapter;
import com.sbi.tapmarket.models.DataDynamicLink;
import com.sbi.tapmarket.models.ModelCustomGrid;
import com.sbi.tapmarket.models.ModelDataUser;
import com.sbi.tapmarket.utils.APIResponse;
import com.sbi.tapmarket.utils.ServicesFactory;
import com.sbi.tapmarket.utils.SessionManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShareDialogActivity extends AppCompatActivity {

    public static final String KEY_TRIAL = "share";
    public static final String WORDING = "wording";
    private String trialId = "";
    private String wording = "";

    ModelCustomGrid[] sosmed = {
            new ModelCustomGrid("WhatsApp", "com.whatsapp", R.drawable.ics_whatsapp, "share_wa"),
            new ModelCustomGrid("Facebook", "com.facebook.katana", R.drawable.ics_facebook, "share_fb"),
            new ModelCustomGrid("Instagram", "com.instagram.android", R.drawable.ics_instagram, "share_ig"),
            new ModelCustomGrid("Line", "jp.naver.line.android", R.drawable.ics_line, "share_line"),
            new ModelCustomGrid("Viber", "com.viber.voip", R.drawable.ics_viber, "share_vb"),
            new ModelCustomGrid("WeChat", "com.tencent.mm", R.drawable.ics_wechat, "share_wc"),
            new ModelCustomGrid("Zalo", "com.zing.zalo", R.drawable.zalo, "share_zl"),
            new ModelCustomGrid("Telegram", "org.telegram.messenger", R.drawable.ics_telegram, "share_tl")};


    ArrayList<ModelCustomGrid> modelCustomGrids = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Window window = this.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.BOTTOM;

        setContentView(R.layout.activity_share_dialog);

        if (savedInstanceState == null) {
            Bundle bundle = getIntent().getExtras();
            if (bundle == null) {
                trialId = null;
                wording = null;
            } else {
                trialId = bundle.getString(KEY_TRIAL);
                wording = bundle.getString(WORDING);
            }
        } else {
            trialId = (String) savedInstanceState.getSerializable(KEY_TRIAL);
            wording = (String) savedInstanceState.getSerializable(WORDING);
        }

        for (ModelCustomGrid modelCustomGrid : sosmed) {
            String name = modelCustomGrid.name;
            String namePackage = modelCustomGrid.package_name;
            int imgCount = modelCustomGrid.icon;
            String source = modelCustomGrid.source;

            if (namePackage.equalsIgnoreCase("sms")) {
                modelCustomGrids.add(new ModelCustomGrid(name, namePackage, imgCount, source));
            } else if (namePackage.equalsIgnoreCase("mailto")) {
                modelCustomGrids.add(new ModelCustomGrid(name, namePackage, imgCount, source));
            } else {
                validateIntent(name, namePackage, imgCount, source);
            }
        }

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        PackageManager manager = getPackageManager();
        List<ResolveInfo> list = manager.queryIntentActivities(sharingIntent, 0);

        RecyclerView mRecyclerView = findViewById(R.id.rv_share_dialog);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        ShareListAdapter adapter = new ShareListAdapter(this, modelCustomGrids, (IBaseAdapterListener<ModelCustomGrid>) (position, data) -> handleClickItem(data));
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, 4));
        mRecyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void validateIntent(String name, String namePackage, int imgCount, String source) {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        List<ResolveInfo> resInfo = getPackageManager().queryIntentActivities(share, 0);
        if (!resInfo.isEmpty()) {
            for (ResolveInfo info : resInfo) {
                if (namePackage.contains(info.activityInfo.packageName.toLowerCase())) {
                    modelCustomGrids.add(new ModelCustomGrid(name, namePackage, imgCount, source));
                    break;
                }
            }
        }
    }

    private void handleClickItem(ModelCustomGrid data) {
        ModelDataUser user = SessionManager.getProfile(this);
        if (user != null) {
            String pName = TextUtils.isEmpty(data.package_name) ? "" : data.package_name;
            String uid = TextUtils.isEmpty(trialId) ? "" : trialId;
            String source = TextUtils.isEmpty(data.source) ? "" : data.source;
            getDynamicLink(pName, uid, source);
        } else {
            Intent toVerification = new Intent(this, PhoneRegister.class);
            startActivity(toVerification);

        }
    }

    private void getDynamicLink(String pName, String source, String id) {
        Call<APIResponse<DataDynamicLink>> call = ServicesFactory.getService().getDynamicShare(source);
        call.enqueue(new Callback<APIResponse<DataDynamicLink>>() {
            @Override
            public void onResponse(@NotNull Call<APIResponse<DataDynamicLink>> call, @NotNull Response<APIResponse<DataDynamicLink>> response) {
                if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()) {
                    DataDynamicLink data = response.body().data;
                    if (data != null) {
                        String link = TextUtils.isEmpty(data.getShortLink()) ? "" : data.getShortLink();
                        doIntent(pName, link, source);
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<APIResponse<DataDynamicLink>> call, @NotNull Throwable t) {
                System.out.println("onFailure.......");
            }
        });
    }

    private void doIntent(String pName, String link, String source) {
        if (pName.equals("mailto")) {
            sendActionEmailShare(link);

        } else if (pName.equals("sms")) {
            sendActionShare(Telephony.Sms.getDefaultSmsPackage(this), link);
        } else {
            sendActionShare(pName, link);
        }
    }

    private void sendActionEmailShare(String link) {
        try {
            if (!TextUtils.isEmpty(link)) {
                String filtered = "Share JudgeMe";
                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "", null));
                intent.putExtra(Intent.EXTRA_SUBJECT, "JudgeMe");
                intent.putExtra(Intent.EXTRA_TEXT, filtered+link);
                startActivity(Intent.createChooser(intent, "Sending Email..."));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendActionShare(String pName, String link) {
        try {
            List<Intent> targetedShareIntents = new ArrayList<>();
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("text/plain");
            List<ResolveInfo> resInfo = getPackageManager().queryIntentActivities(share, 0);
            if (resInfo != null && resInfo.size() > 0) {
                for (ResolveInfo info : resInfo) {
                    String shareBody = wording;
                    Intent targetedShare = new Intent(Intent.ACTION_SEND);
                    if (pName.contains(info.activityInfo.packageName.toLowerCase())) {
                        targetedShare.setType("text/plain");
                        targetedShare.putExtra(Intent.EXTRA_TEXT, shareBody+link);
                        targetedShare.setPackage(info.activityInfo.packageName.toLowerCase());
                        targetedShareIntents.add(targetedShare);
                    }
                }

                if (targetedShareIntents.size() > 0) {
                    Intent chooserIntent = Intent.createChooser(targetedShareIntents.remove(0), "JudgeMe Share");
                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[targetedShareIntents.size()]));
                    startActivity(chooserIntent);
                } else {
                    Toast.makeText(getApplicationContext(), "App not installed", Toast.LENGTH_SHORT).show();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
