package com.sbi.tapmarket.models;

public class ModelDataUser {

    public String msisdn;
    public String user_id;
    public String nickname;
    public String token;

    public ModelDataUser() {}

    public ModelDataUser(String msisdn, String user_id, String nickname, String token) {
        this.msisdn = msisdn;
        this.user_id = user_id;
        this.nickname = nickname;
        this.token = token;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
