package com.sbi.tapmarket.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PackageDetail implements Serializable {

    @SerializedName("type")
    private String name;
    @SerializedName("value")
    private String kuota;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKuota() {
        return kuota;
    }

    public void setKuota(String kuota) {
        this.kuota = kuota;
    }

    @Override
    public String toString() {
        return "PackageDetail{" +
                "name='" + name + '\'' +
                ", kuota='" + kuota + '\'' +
                '}';
    }
}
