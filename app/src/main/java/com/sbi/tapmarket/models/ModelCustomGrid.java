package com.sbi.tapmarket.models;

public class ModelCustomGrid {
    public String name;
    public String package_name;
    public int icon;
    public String source;

    public ModelCustomGrid(String name, String package_name, int icon, String source) {
        this.name = name;
        this.package_name = package_name;
        this.icon = icon;
        this.source = source;
    }

    public String getName() {
        return name;
    }

    public String getPackage_name() {
        return package_name;
    }

    public int getIcon() {
        return icon;
    }

    public String getSource() {
        return source;
    }
}
