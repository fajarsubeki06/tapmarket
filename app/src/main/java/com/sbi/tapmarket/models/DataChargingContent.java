package com.sbi.tapmarket.models;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;


public class DataChargingContent implements Serializable {

    @SerializedName("keyword")
    private String keyword;
    @SerializedName("unique_id")
    private String uniq_id;
    @SerializedName("sdc")
    private String sdc;
    @SerializedName("price")
    private String price;
    @SerializedName("currency")
    private String currency;
    @SerializedName("channel")
    private String channel;
    @SerializedName("package_id")
    private String package_id;
    @SerializedName("package_detail")
    private ArrayList<PackageDetail> packageDetail;

    public DataChargingContent() {
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getUniq_id() {
        return uniq_id;
    }

    public void setUniq_id(String uniq_id) {
        this.uniq_id = uniq_id;
    }

    public String getSdc() {
        return sdc;
    }

    public void setSdc(String sdc) {
        this.sdc = sdc;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getPackage_id() {
        return package_id;
    }

    public void setPackage_id(String package_id) {
        this.package_id = package_id;
    }

    public ArrayList<PackageDetail> getPackageDetail() {
        return packageDetail;
    }

    public void setPackageDetail(ArrayList<PackageDetail> packageDetail) {
        this.packageDetail = packageDetail;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return "DataChargingContent{" +
                "keyword='" + keyword + '\'' +
                ", uniq_id='" + uniq_id + '\'' +
                ", sdc='" + sdc + '\'' +
                ", price='" + price + '\'' +
                ", currency='" + currency + '\'' +
                ", channel='" + channel + '\'' +
                ", package_id='" + package_id + '\'' +
                ", packageDetail=" + packageDetail +
                '}';
    }
}
