package com.sbi.tapmarket.adapter;

public interface IBaseAdapterListener<T> {
    void onItemClick(int position, T data);
}
