package com.sbi.tapmarket.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sbi.tapmarket.R;
import com.sbi.tapmarket.models.DataChargingContent;
import java.util.ArrayList;


public class PopUpSubsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    final private Context context;
    private ArrayList<DataChargingContent> dataPopUpSubs;
    private ProgressDialog progressDialog;
    private String price_label = "";

    public PopUpSubsAdapter(ArrayList<DataChargingContent> dataPopUpSubs, Context context) {
        this.dataPopUpSubs = dataPopUpSubs;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.item_radio_subs, parent, false);

        progressDialog = new ProgressDialog(v.getContext());
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage("Processing...");
        progressDialog.setIndeterminate(true);
        return new VH(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final DataChargingContent data = dataPopUpSubs.get(position);
        final VH vh = (VH) holder;

        try {
            price_label = TextUtils.isEmpty(data.getPrice()) ? "" : data.getPrice();
        }catch (Exception e){
            e.printStackTrace();
        }

        vh.radio.setText(data.getCurrency() + " " +  price_label);

        LinearLayoutManager layout = new LinearLayoutManager(context);
        layout.setOrientation(LinearLayoutManager.VERTICAL);
        vh.recyclerPackageDetail.setLayoutManager(layout);
        PackageDetailAdapter ar = new PackageDetailAdapter(data, context);
        vh.recyclerPackageDetail.setAdapter(ar);
        vh.recyclerPackageDetail.getAdapter().notifyDataSetChanged();

        vh.btnSubs.setText(context.getString(R.string.text_choose));
        vh.btnSubs.setOnClickListener(view -> {
            eventClick(data);
        });

        vh.relativeLayout.setOnClickListener(view -> {
           eventClick(data);
        });
    }

    private void eventClick(DataChargingContent data){

        String channel = TextUtils.isEmpty(data.getChannel())?"":data.getChannel();
        String keyword = TextUtils.isEmpty(data.getKeyword())?"":data.getKeyword();
        String number = TextUtils.isEmpty(data.getSdc())?"":data.getSdc();
        String package_id = TextUtils.isEmpty(data.getPackage_id())?"":data.getPackage_id();
        String uniq_id = TextUtils.isEmpty(data.getUniq_id())?"":data.getUniq_id();

        if (channel.equals("compose")){
            Uri uri = Uri.parse("smsto:" + number);
            Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
            intent.putExtra("sms_body", keyword + " " + package_id+" "+uniq_id);
            context.startActivity(intent);
            ((Activity) context).finish();
        }

    }

    @Override
    public int getItemCount() {
        return dataPopUpSubs.size();
    }

    public class VH extends RecyclerView.ViewHolder {
        TextView radio;
        RelativeLayout relativeLayout;
        RecyclerView recyclerPackageDetail;
        Button btnSubs;

        public VH(View itemView) {
            super(itemView);
            radio = itemView.findViewById(R.id.rbSubs);
            relativeLayout = itemView.findViewById(R.id.rPriceList);
            recyclerPackageDetail = itemView.findViewById(R.id.recyclerPackageDetail);
            btnSubs = itemView.findViewById(R.id.btnSubs);
        }
    }

}