package com.sbi.tapmarket.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sbi.tapmarket.R;
import com.sbi.tapmarket.models.DataChargingContent;
import com.sbi.tapmarket.models.PackageDetail;

import java.util.ArrayList;

public class PackageDetailAdapter extends RecyclerView.Adapter<PackageDetailAdapter.ViewHolder>{

    private Context context;
    private DataChargingContent data;

    public PackageDetailAdapter(DataChargingContent packageDetails, Context context) {
        this.context = context;
        this.data = packageDetails;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.item_package_detail, parent, false);

        return new ViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final ArrayList<PackageDetail> dataVideoDinamic = data.getPackageDetail();
        final PackageDetail packageDetail = dataVideoDinamic.get(position);


        String name = TextUtils.isEmpty(packageDetail.getName())?"":packageDetail.getName();
        String kuota = TextUtils.isEmpty(packageDetail.getKuota())?"":packageDetail.getKuota();

        holder.textPackageName.setText(kuota +  " " + name);
    }

    @Override
    public int getItemCount() {
        if (data.getPackageDetail() == null){
            return 0;
        }else if (data.getPackageDetail().size() <= 9){
            return data.getPackageDetail().size();
        }else {
            return data.getPackageDetail().size()+1;
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        TextView textPackageName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            textPackageName = itemView.findViewById(R.id.text_packagename);
        }
    }

}
