package com.sbi.tapmarket.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.sbi.tapmarket.R;
import com.sbi.tapmarket.models.ModelCustomGrid;

import java.util.ArrayList;

public class ShareListAdapter extends RecyclerView.Adapter<ShareListAdapter.ShareHolder> {

    private Activity context;
    private ArrayList<ModelCustomGrid> items;
    private IBaseAdapterListener listener;

    public ShareListAdapter(Activity context, ArrayList<ModelCustomGrid> items, IBaseAdapterListener listener) {
        this.context = context;
        this.items = items;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ShareHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_share, parent, false);
        return new ShareHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ShareHolder holder, int position) {
        Glide.with(context)
                .load(items.get(position).icon)
                .into(holder.mImageShare);

        holder.mTvShare.setText(items.get(position).name);
        holder.itemView.setOnClickListener(v -> listener.onItemClick(position, items.get(position)));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ShareHolder extends RecyclerView.ViewHolder {
        private ImageView mImageShare;
        private TextView mTvShare;

        ShareHolder(@NonNull View itemView) {
            super(itemView);

            mImageShare = itemView.findViewById(R.id.shareImage);
            mTvShare = itemView.findViewById(R.id.shareName);
        }
    }
}
