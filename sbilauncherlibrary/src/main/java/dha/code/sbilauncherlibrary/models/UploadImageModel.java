package dha.code.sbilauncherlibrary.models;


import android.text.TextUtils;

public class UploadImageModel {

    private String url;

    public UploadImageModel() {
    }

    public String getUrl() {
        return TextUtils.isEmpty(url)?"":url;
    }

}