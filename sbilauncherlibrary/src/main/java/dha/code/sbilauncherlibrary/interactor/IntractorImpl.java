package dha.code.sbilauncherlibrary.interactor;

import android.content.Context;
import android.text.TextUtils;

import java.util.Collections;
import java.util.List;

import dha.code.sbilauncherlibrary.contract.MainContract;
import dha.code.sbilauncherlibrary.models.UploadImageModel;
import dha.code.sbilauncherlibrary.retrofit.APIResponse;
import dha.code.sbilauncherlibrary.retrofit.ServicesFactory;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class IntractorImpl implements MainContract.Intractor {

    @Override
    public void uploadImageFile(Context context, final MultipartBody.Part file, RequestBody msisdn, final OnUploadListener onUploadListener) {
        onUploadListener.onProgress();
        Call<APIResponse<UploadImageModel>> call = ServicesFactory.getService().updatePhoto("bearer eyJhcHBfYWxpYXMiOiJ1dGVuZ2dvIiwiYXBwX25hbWUiOiJVbGFyIFRlbmdnbyIsImNwX2lkIjoxfQ==aGeWlvxnnXLxKA7fMKH1",
                msisdn, file);
        call.enqueue(new Callback<APIResponse<UploadImageModel>>() {
            @Override
            public void onResponse(Call<APIResponse<UploadImageModel>> call, Response<APIResponse<UploadImageModel>> response) {
                if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()){
                    UploadImageModel mModel = response.body().data;
                    if (mModel != null){
                        String _url = mModel.getUrl();
                        if (TextUtils.isEmpty(_url))
                            return;

                        onUploadListener.onSuccess(_url);
                        return;
                    }
                }
                onUploadListener.onFailure();
            }

            @Override
            public void onFailure(Call<APIResponse<UploadImageModel>> call, Throwable t) {
                onUploadListener.onFailure();
            }
        });
    }

}
